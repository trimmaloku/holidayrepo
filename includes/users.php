<?php
session_start();
if(isset($_SESSION['id']) && $_SESSION['role'] === 'Admin'){
?>

<?php
    require 'connect.php';

    $query = $pdo->query('SELECT * FROM users');
    $users = $query->fetchAll();
?>


<div class="container">
    <table>
        <thead>
        
            <th>User id:</th>
            <th>Emri:</th>
            <th>Emaili:</th>
            <th>Passwordi:</th>
        
        </thead>

        <tbody>
            <?php foreach($users as $user):?>
            <tr>
                <td><?php echo $user['id'] ?></td>
                <td><?php echo $user['name'] ?></td>
                <td><?php echo $user['email'] ?></td>
                <td><?php echo $user['password'] ?></td>
                <td><a href="edit_users.php?id=<?php echo $user['id']; ?>">Edit</a></td>
                <td><a href="delete_user.php?id=<?php echo $user['id']; ?>">Delete</a></td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</div>

<?php } else{
    header("Location: ../index.php");
}

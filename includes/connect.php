<?php

    $server   = "localhost";
    $username = "root";
    $password = "";
    $database = "holidaysdb";

    try {
        $pdo = new PDO("mysql:host=$server;dbname=holidaysdb", $username, $password);
        // set the PDO error mode to exception
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      } catch(PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
      }
?>
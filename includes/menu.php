<?php session_start(); ?>
<!-- The Modal -->
<div id="myModal" class="modal"></div>

<nav>
    <a href="index.php"><img src="images/Oscar-Holidays.png" width="150px"></a>
    
    <img src="images/ham.svg" id="ham" class="hide-desktop show-mobile">

    <ul id="menu" class="hide-mobile show-desktop">
        <img src="images/exit.svg" id="exit" class="hide-desktop show-mobile">
        <a href="index.php"><li>Home</li></a>
        <a href="about.php"><li>About Us</li></a>
        <a href="contact.php"><li>Contact</li></a>
    <?php if(isset($_SESSION['name'])): ?>
        <a href="profile.php?id=<?php echo $_SESSION['id']; ?>"><li>Miresevini, <?php echo $_SESSION['name'];?></li></a>
        <a href="includes/logout.php"><li>Log out</li></a>
    <?php else: ?>
        <a href="login.php"><li>Login/Register</li></a>
    <?php endif;?>
    </ul>
</nav>
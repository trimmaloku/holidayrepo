<?php
include 'includes/connect.php';
session_start();
if(isset($_SESSION['id']) && $_SESSION['role'] === 'Admin'){
?>

<?php

    $query = $pdo->prepare('SELECT * FROM booking');
    $query->execute();

    ?>

<table id="booking-table">
    <link rel="stylesheet" href="css/style.css" />
        <tr>
            <th>ID</th>
            <th>Destinacioni</th>
            <th>Hoteli</th>
            <th>Muaji</th>
            <th>Dita</th>
            <th>Rezervuar nga</th>
        </tr>
    
        <?php while($row = $query->fetch()){ ?>

        <tr>
            <td><?php echo $row['id'];?></td>
            <td><?php echo $row['destinacioni'];?></td>
            <td><?php echo $row['hoteli'];?></td>
            <td><?php echo $row['muaji'];?></td>
            <td><?php echo $row['dita'];?></td>
            <td><?php echo $row['userid'];?></td>
        </tr>

        <?php } ?>
</table>

<!-- html -->


<?php } else{
    header("Location: index.php");
}
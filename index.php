<!DOCTYPE html>
<html lang="en">

    <?php include("includes/header.php"); ?>

<body>
    <?php include("includes/menu.php");
    
    if(isset($_SESSION['id'])){}

    $query = $pdo->prepare('SELECT * FROM destinacionet');
    $query2 = $pdo->prepare('SELECT * FROM hotelet');
    $query3 = $pdo->prepare('SELECT * FROM muajt');
    $query4 = $pdo->prepare('SELECT * FROM ditet');
    $query->execute();
    $query2->execute();
    $query3->execute();
    $query4->execute();
    ?>

    <?php
    
    if(isset($_POST['submit'])){
        $destinacioni = $_POST['destinacioni'];
        $hoteli = $_POST['hoteli'];
        $muaji = $_POST['muaji'];
        $dita = $_POST['dita'];
        $userid = $_SESSION['id'];
        $sql ="INSERT INTO booking (destinacioni,hoteli,muaji,dita,userid) VALUES (:destinacioni,:hoteli,:muaji,:dita,:userid)";
        $query = $pdo->prepare($sql);
        $query->bindParam('destinacioni',$destinacioni);
        $query->bindParam('hoteli',$hoteli);
        $query->bindParam('muaji',$muaji);
        $query->bindParam('dita',$dita);
        $query->bindParam('userid',$userid);
        $query->execute();
        header("Location: profile.php");
    }

    ?>
    

    <section id="first-section">
        <div id="promo-mini">
            <div id="rolling-promo">
                
                <div id="butonat">
                    <button onclick="prev()" id="prev">Prapa</button>
                    <button onclick="next()" id="next">Para</button>
                </div>
                <img id="slider" src="images/Optimized-aerial.jpg">
            </div>

            <div id="mini-promos">
                <div class="mini-promo">
                    <img src="images/Optimized-aerial.jpg">
                </div>
    
                <div class="mini-promo">
                    <img src="images/Optimized-sea.jpg">
                </div>
    
                <div class="mini-promo">
                    <img src="images/Optimized-palm.jpg">
                </div>
    
                <div class="mini-promo">
                    <img src="images/Optimized-island.jpg">
                </div>
            </div>
            
        </div>

    <div id="booking-offer">  
        <div id="booking">
            <h1>Book now</h1>

        <div id="opsionet">
            <form id="booking-form" action="#" method="post" onsubmit="return checkforblank()">
                
            <!-- destinacioni -->
            <label class="fillErrorDest fillError">Mbusheni kete fushe!</label>
            <select id="destinacioni" name="destinacioni" onchange="checkforblankDestination()">
                <option value="Zgjedh destinacionin" selected disabled hidden>Zgjedh destinacionin</option>
                <?php while($row = $query->fetch()){ ?>
                <option value="<?php echo $row["destinacioni"];?>"><?php echo $row["destinacioni"];}?></option>
            </select>

            <!-- hoteli -->
            <label class="fillErrorHot fillError">Mbusheni kete fushe!</label>
            <select id="hoteli" name="hoteli" onchange="checkforblankHotel()">
                <option value="Zgjedh hotelin" selected disabled hidden>Zgjedh hotelin</option>
                <?php while($row = $query2->fetch()){ ?>
                <option value="<?php echo $row["hoteli"];?>"><?php echo $row["hoteli"];}?></option>
            </select>

            <!-- muaji -->
            <label class="fillErrorM fillError">Mbusheni kete fushe!</label>
            <select id="muaji" name="muaji" onchange="checkforblankMuaji()">
                <option value="Zgjedh muajin" selected disabled hidden>Zgjedh muajin</option>
                <?php while($row = $query3->fetch()){ ?>
                <option value="<?php echo $row["muaji"];?>"><?php echo $row["muaji"];}?></option>
            </select>
            
            <!-- dita -->
            <label class="fillErrorD fillError">Mbusheni kete fushe!</label>
            <select id="dita" name="dita" onchange="checkforblankDita()">
                <option value="Zgjedh diten" selected disabled hidden>Zgjedh diten</option>
                <?php while($row = $query4->fetch()){ ?>
                <option value="<?php echo $row["dita"];?>"><?php echo $row["dita"];}?></option>
            </select>

            <button type="submit" name="submit" id="rezervo">Rezervo tani</button>

            </form>
        </div>


        </div>
    
        <div id="offer">
            <h1>Oferta e dites</h1>
            <div id="offer-text">
                <h3>Prishtine - Tirane</h3>
                <p>Nisja nga ora 09:00</p>
            </div>
        </div>
    </div>  
</section>
</body>

<?php include("includes/footer.php") ?>
<script src="js/bookingValidation.js"></script>

</html>
<?php
session_start();
if(isset($_SESSION['id']) && $_SESSION['role'] === 'Admin'){
?>

<?php
    include 'includes/connect.php';

    $query = $pdo->prepare('SELECT * FROM articles');
    $query->execute();

    while($row = $query->fetch()){
?>
        <h2><?php echo $row["title"]; ?> </h2>
        <p>Descriptioni: <br> <?php echo $row["body"]; ?> </p>
        <h4>Shkruar nga <br> <?php echo $row["personi"]; ?> </h4>

        <hr>
<?php
    }

?>

<?php } else{
    header("Location: index.php");
}
<!DOCTYPE html>
<html lang="en">

    <?php include("includes/header.php"); ?>
    <link rel="stylesheet" href="css/about.css">

<body>
    <?php include("includes/menu.php"); ?>
<?php
    $query = $pdo->prepare('SELECT * FROM articles');
    $query2 = $pdo->prepare('SELECT * FROM faq');
    $query->execute();
    $query2->execute();

    
?>
    <section id="ngj1">
        <div id="about">
            <div id="first-pic-text">
                <img src="images/Optimized-airline.jpg" alt="">
                <div id="text">
                <?php $row = $query->fetch() ?>
                    <?php echo $row["body"];?>
                </div>
            </div>

            <div id="second-pic-faq">
                <img src="images/Optimized-bus.jpg" alt="">

                <div id="faq">
                    <?php while($row = $query2->fetch()){ ?>
                        <button class="accordion"><?php echo $row["question"];?></button>
                            <div class="panel">
                                <p><?php echo $row["answer"];?></p>
                            </div>
                    <?php
                        }
                    ?>
                </div>
            </div>
        </div>
    </section>

    <section id="ngj2">
        <h1 id="stafi-text">STAFI YNE</h1>
        <div id="stafi">
            <div class="staff" >
                <img src="images/ceo.jpg" alt="">
                <div class="content">
                    <h1>CEO</h1>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Esse, dolores.</p>
                </div>
            </div>

            <div class="staff">
                <img src="images/Optimized-founder.jpg" alt="">
                <div class="content">
                    <h1>Co-Founder</h1>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Esse, dolores.</p>
                </div>
            </div>

            <div class="staff">
                <img src="images/Optimized-security.jpg" alt="">
                <div class="content">
                    <h1>Security</h1>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Esse, dolores.</p>
                </div>
            </div>
        </div>

    </section>
</body>

<?php include("includes/footer.php") ?>
<script src="js/accordion.js"></script>

</html>

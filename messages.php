<?php
session_start();
if(isset($_SESSION['id']) && $_SESSION['role'] === 'Admin'){
?>

<?php
    include 'includes/connect.php';

    $query = $pdo->prepare('SELECT * FROM contacts');
    $query->execute();

    while($row = $query->fetch()){
?>


<!-- html -->
<link rel="stylesheet" href="css/style.css" />

<table id="booking-table">
        <tr>
            <th>ID</th>
            <th>Emri:</th>
            <th>Emaili:</th>
            <th>Komenti:</th>
        </tr>

        <tr>
            <td><?php echo $row["id"]; ?></td>
            <td><?php echo $row["name"]; ?></td>
            <td><?php echo $row["email"]; ?></td>
            <td><?php echo $row["comment"]; ?></td>
        </tr>
</table>

        <hr>

<?php
    }

?>

<?php } else{
    header("Location: index.php");
}
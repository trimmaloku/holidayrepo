<?php
    include 'includes/header.php';
    include 'includes/menu.php';

    if(isset($_SESSION['id'])){

    $sql = 'SELECT * from users WHERE id = :id';
    $query = $pdo->prepare($sql);
    $query->execute(['id' => $_SESSION['id']]);
    
    $user = $query->fetch();

    $query = $pdo->prepare("SELECT * FROM booking WHERE userid = '".$_SESSION['id']."'");
    $query->execute();

    if(isset($_POST['fshij'])){
        $key = $_POST['keyToDelete'];
        $queryDelete = $pdo->prepare("DELETE FROM booking where id ='".$key."'");
        $queryDelete->execute();
    }

    // edit,delete acc

    if(isset($_POST['fshijAcc'])){
        $sql = 'DELETE FROM users WHERE id = :id';
        $query = $pdo->prepare($sql);
        $query->execute(['id' => $_SESSION['id']]);
        session_destroy();
        header("Location: index.php");
    }

    if(isset($_POST['editoAcc'])){
        if(isset($_POST['name'])){
            $name=$_POST['name'];
        }
        if(isset($_POST['email'])){
            $email=$_POST['email'];
        }
        if(isset($_POST['password'])){
            $password=password_hash($_POST['password'], PASSWORD_BCRYPT);
        }

    
        $sqlEdit ="UPDATE users SET name = :name, email = :email, password = :password WHERE id = '".$_SESSION['id']."'";
        $queryEdit = $pdo->prepare($sqlEdit);
        $queryEdit->bindParam('name',$name);
        $queryEdit->bindParam('email',$email);
        $queryEdit->bindParam('password',$password);
        
        $queryEdit->execute();
    }

    // profile image
    if(isset($_FILES['profileImage']['name'])){
        $fotoja = $_FILES['profileImage']['name'];  
    }
    if(isset($_FILES['profileImage']['tmp_name'])){
        $temp_name = $_FILES['profileImage']['tmp_name']; 
    }

    if(isset($_POST['change_pic'])){     
        echo "<pre>",print_r($fotoja),"</pre>";
        $profileImage = time() . '_' . $fotoja;
    
        $target = "../images";
        $target_file = $target.basename($profileImage);
    
        if(move_uploaded_file($temp_name,$target_file)){
            $sql = "UPDATE users SET profileImage = :profileImage WHERE id = '".$_SESSION['id']."'";
            $query = $pdo->prepare($sql);
            $query->bindParam('profileImage',$profileImage);
            $query->execute();
            echo "Uploaded successfully!";
        }

        else{
            echo "failed to upload";
        }
    }
        
?>
<?php if($_SESSION['role'] === 'User'){ ?>
<!-- User dashboard -->
<link rel="stylesheet" href="css/style.css" />
<div id="container">
    <div id="profile-row" class="row">
        <div id="left-row">
            <h2>Edito te dhenat</h2>

            <form method="POST" action="">

            <label for="emri">Emri </label><br />
            <input type="text" id="emri" name="name" value="<?php echo $user['name'];?>" placeholder="Ndrysho Emrin" />
            <br />

            <label for="emri">Email </label><br />
            <input type="email" id="email" name="email" value="<?php echo $user['email'];?>" placeholder="Ndrysho Email-in" />
            <br />

            <label for="password">Password </label><br />
            <input type="password" id="password" name="password" placeholder="Ndrysho Password-in" />
            <br />

            <div id="buttons">
                <input type="submit" name="editoAcc" value="Edito"><br>
                <input type="submit" name="fshijAcc" value="Fshij accountin">
            </div>

            </form>
        </div>

        <div id="right-row">
            <form method="post" action="" enctype="multipart/form-data">
                <img src="<?php echo '/images' . $user['profileImage']; ?>" onClick="triggerClick()" id="profileDisplay" style="border-radius:50%"><br>
                <label>Klikom lart per te zgjedhur foto</label>
                <input id="change-pic" type="file" name="profileImage" onChange="displayImage(this)" style="display:none;"><br/>
                <button style="width:85%;" name="change_pic" type="submit">Uploado foton</button>
                <script src="js/profileimg.js"></script>
            </form>
        </div>
    </div>

    <hr>

    <div id="bookings">
        <div class="row">
            <h1>Bookings</h1>

            <table id="booking-table">
                <form method="post" action="">
                <tr>
                    <th>ID</th>
                    <th>Destinacioni</th>
                    <th>Hoteli</th>
                    <th>Muaji</th>
                    <th>Dita</th>
                    <th>Selekto</th>
                    <th>Settings</th>
                </tr>
                
                <?php while($row = $query->fetch()){ ?>

                <tr>
                    <td><?php echo $row['id'];?></td>
                    <td><?php echo $row['destinacioni'];?></td>
                    <td><?php echo $row['hoteli'];?></td>
                    <td><?php echo $row['muaji'];?></td>
                    <td><?php echo $row['dita'];?></td>
                    <td><input type="checkbox" name="keyToDelete" value="<?php echo $row['id'];?>"></td>
                    <td><input type="submit" name="fshij" value="Fshij"></td>
                </tr>

                <?php } ?>
                </form>
            </table>
        </div>
    </div>
</div>
<?php }else{ ?>
<!-- Admin dashboard -->
<div id="container">
    <div id="profile-row" class="row">
        <div id="left-row">
            <h1>Admin dashboard</h1>
        </div>
        
        <div style="text-align:center;">
            <a href="includes/add_users.php"><button type="submit">Shto usera</button></a>
            <a href="includes/delete_user.php"><button type="submit">Edito/Fshij usera</button></a>
            <a href="showdata.php"><button type="submit">Shiko artikujt</button></a>
            <a href="bookings.php"><button type="submit">Shiko bookings</button></a>
            <a href="messages.php"><button type="submit">Kontaktet/Mesazhet</button></a>
            <a href="add-data.php"><button type="submit">Shto artikull</button></a>
        </div>
    </div>
</div>

<?php }?>

<?php 
    include 'includes/footer.php';
?>

<?php } else{
    header("Location: index.php");
}



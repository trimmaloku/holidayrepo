<!DOCTYPE html>
<html lang="en">
    <?php 
        include("includes/header.php");
        // include("includes/menu.php");
        session_start();
        // if logged
        if(isset($_SESSION['id'])){
            header("Location: index.php");
        }
    
        if(isset($_POST['submitSignUp'])){
            $name = $_POST['name'];
            $email = $_POST['email'];
            $role = 'User'; //default me kon user i regjistrumi
            $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
    
            $sql ='INSERT INTO users (name,email,role,password) VALUES (:name,:email,:role,:password)';
            $query = $pdo->prepare($sql);
            $query->bindParam('name',$name);
            $query->bindParam('email',$email);
            $query->bindParam('role',$role);
            $query->bindParam('password',$password);
    
            try{
                $query->execute();
                $message = "u krijua useri!";
            }
            catch(PDOException $e){
                $message = $e;
            }
        }



        if(!empty($message)){
            ?><h3><?$message ?></h3>
        <?php
        }
    ?>

    <?php
        if(isset($_POST['submitLogin'])):
            $email = $_POST['email'];
            $password = $_POST['password'];
            $role = $_POST['role'];

            $query = $pdo->prepare('SELECT id,name,email,password,role FROM users WHERE email=:email');
            $query->bindParam(':email',$email);
            $query->execute();
            $user = $query->fetch();

            if(count($user) > 0 && password_verify($password,$user['password']) && $user['role'] === 'User'){
                $_SESSION['id'] = $user['id'];
                $_SESSION['name'] = $user['name'];
                $_SESSION['email'] = $user['email'];
                $_SESSION['role'] = $user['role'];
                header("Location: profile.php");
            }
            else if(count($user) > 0 && password_verify($password,$user['password']) && $user['role'] === 'Admin'){
                $_SESSION['id'] = $user['id'];
                $_SESSION['name'] = $user['name'];
                $_SESSION['email'] = $user['email'];
                $_SESSION['role'] = $user['role'];
                header("Location: profile.php");
            }
            else{
                echo "Gabim!";
            }
        endif;
    ?>      
    <!--  css  -->
    <link rel="stylesheet" href="css/login.css">
<body>
        <div id="box">
            <div id="btn-box">
                <div id="btn"></div>
                <button type="button" class="toggle-btn" onclick="logini()">Log In</button>
                <button type="button" class="toggle-btn" onclick="signup()">Sign Up</button>
            </div>
            
            <div class="ikonat">
                <a href="https://www.facebook.com" target="_blank"><img src="images/fb.png"></a>
                <a href="https://www.twitter.com" target="_blank"><img src="images/tw.png"></a>
                <a href="https://www.gmail.com" target="_blank"><img src="images/gp.png"></a>
            </div>

            <form action="login.php" id="login" method="post" class="inputi" onsubmit="return validateLogin()">
                <input id="emaili" name="email" type="email" class="input-field" placeholder="Email">
                <input id="passwordi" name="password" type="password" class="input-field" placeholder="Password">
                <div id="box-text"><input name="checkbox" type="checkbox" class="check-box"><span id="remember">Remember Password</span></div>
                <button type="submit" name="submitLogin" class="submit-btn">Login</button>
            </form>


    
            <form id="sign-up" method="post" class="inputi" onsubmit="return validateSignUp()">
                <input id="name" name="name" type="text" class="input-field" placeholder="Name">
                <input id="emaili2" name="email" type="text" class="input-field" placeholder="Email">
                <input id="passwordi2" name="password" type="password" class="input-field" placeholder="Password (8 karaktere)">
                <div id="box-text"><input id="checkboxi" type="checkbox" class="check-box"><span id="checkbox-text">I agree to the <div id="pointeri"> terms & conditions</div></span></div>
                <button type="submit" name="submitSignUp" class="submit-btn">Sign up</button>
            </form>

        </div>

        <!-- Modali -->
        <div class="bg-modal">
            <div class="modal-content">
                <div class="close">+</div>
                <h3>Terms & Conditions</h3>
                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ex quis natus assumenda sequi debitis amet commodi quod iusto vero aut error dolor iste repudiandae doloribus, cupiditate repellendus ducimus quasi nesciunt asperiores accusamus adipisci nam veritatis excepturi blanditiis! Accusamus vel reiciendis officiis ut eos, nam voluptas necessitatibus cum ex numquam debitis excepturi nemo impedit, officia eum explicabo velit placeat. Autem vitae exercitationem fuga architecto ab eum, aut libero explicabo maiores doloribus pariatur.</p>
            </div>
        </div>

</body>

<script src="js/login.js"></script>
<script src="js/menu.js"></script>

</html>
<!DOCTYPE html>
<html lang="en">

    <?php include("includes/header.php"); ?>
    <link rel="stylesheet" href="css/contact.css">

<body>
    <?php include("includes/menu.php");
    
    if(isset($_POST['dergo'])){
        $name = $_POST['name'];
        $email = $_POST['email'];
        $comment = $_POST['comment'];
        $sql = "INSERT INTO contacts (name,email,comment) VALUES (:name,:email,:comment)";
        $query = $pdo->prepare($sql);
        $query->bindParam('name',$name);
        $query->bindParam('email',$email);
        $query->bindParam('comment',$comment);
        
        try{
            $query->execute();
            $message = "U dergua mesazhi!";
        }
        catch(PDOException $e){
            $message = $e;
        }
    }

    ?>

    <section id="ngj">
        <div id="contact">

            <div id="map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6978.6460625143245!2d21.152223772629576!3d42.65798104368919!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x13549ee56f851e0b%3A0xe6824045d06ee9c5!2sPashko%20Vasa%2C%20Prishtin%C3%AB!5e0!3m2!1sen!2s!4v1589394458945!5m2!1sen!2s" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>

            <div id="contact-form">

                <div id="contact-details">
                    <h3>Kontakti</h3>
                    <h4>Na kontakto sot, dhe merr nje pergjigjje brenda 24 oresh!</h4>
                </div>

                <form id="contactForm" method="POST" onsubmit="return validateForm()">

                    <label for="emri"><b>Emri</b></label>
                    <label id="fillErrorName">Mbusheni kete fushe!</label>
                    <input type="text" id="emri" name="name" placeholder="Emri juaj..." tabindex="1">
                
                    <label for="email"><b>Email</b></label>
                    <label id="fillErrorEmail">Mbusheni kete fushe!</label>
                    <input type="email" id="email" name="email" placeholder="Emaili juaj..." tabindex="2">
                
                    <label for="koment"><b>Koment</b></label>
                    <label id="fillErrorKoment">Mbusheni kete fushe!</label>
                    <textarea id="koment" name="comment" placeholder="Komenti juaj ketu..." tabindex="3" rows="3"></textarea>
                
                    <button type="submit" name="dergo" id="submit-button" tabindex="4">Dërgo</button>
                
                </form>
            </div>
        </div>
    </section>
</body>

<?php include("includes/footer.php") ?>
<script src="js/contactValidation.js"></script>

</html>
var menu = document.getElementById('menu');
var ham = document.getElementById('ham');
var exit = document.getElementById('exit');
var body = document.querySelector('body');

ham.addEventListener('click', function(){
    menu.classList.toggle('hide-mobile');
    body.style.overflow = "hidden";
});

exit.addEventListener('click', function(){
    menu.classList.add("hide-mobile");
    body.style.overflow = "visible";
});


var modal = document.getElementById("myModal");

ham.onclick = function() {
  modal.style.display = "block";
}

exit.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
    menu.classList.add("hide-mobile");
    body.style.overflow = "visible";
  }
}
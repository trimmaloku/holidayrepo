var x = document.getElementById("login");
var y = document.getElementById("sign-up");
var z = document.getElementById("btn");

function validateLogin() {
    var imella = document.forms["login"]["email"].value;
    var imellaInput = document.getElementById("emaili");

    var pwd = document.forms["login"]["password"].value;
    var pwdInput = document.getElementById("passwordi");

    // imella validation

    if ((imella.trim() == "" || imella == null) || !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(imella))) {
        imellaInput.style.borderBottom = '1px solid red';
        return false;
    }
    else{
        imellaInput.style.borderBottom = '1px solid #999';
    }

    // passwordi validation
    if (pwd.trim() == "" || pwd == null) {
        pwdInput.style.borderBottom = '1px solid red';
        return false;  
    }
    else if(pwd.length < 8){
        return false;
    }
    else{
        pwdInput.style.borderBottom = '1px solid #999';
    }

}

function validateSignUp(){

    var name = document.forms["sign-up"]["name"].value;
    var nameInput = document.getElementById("name");

    var imella = document.forms["sign-up"]["email"].value;
    var imellaInput = document.getElementById("emaili2");

    var pwd = document.forms["sign-up"]["password"].value;
    var pwdInput = document.getElementById("passwordi2");

    var boxi = document.getElementById("checkboxi");
    var teksti = document.getElementById("pointeri");

    // emri validation

    if(name.trim() == "" || name == null){
        nameInput.style.borderBottom = '1px solid red';
        return false;
    }
    else{
        nameInput.style.borderBottom = '1px solid #999';
    }

    // emaili validation

    if ((imella.trim() == "" || imella == null) || !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(imella))) {
        imellaInput.style.borderBottom = '1px solid red';
        return false;
    }
    else{
        imellaInput.style.borderBottom = '1px solid #999';
    }

    // passwordi validation

    var upperCase = /^[A-Z]*$/;
    // nese o me tmdhaja,ma e vogel se 8 ose that -> error 
    if(pwd.match(upperCase) || pwd.length < 8 || pwd.trim() == "") {
        pwdInput.style.borderBottom = '1px solid red';
        return false;
    } else {
        pwdInput.style.borderBottom = '1px solid #999';
    }

    if(!boxi.checked){
        teksti.style.textDecoration = 'unset';
        teksti.style.borderBottom = '1px solid red';
        return false;
    }
    else{
        teksti.style.textDecoration = 'underline';
        teksti.style.borderBottom = 'unset';
    }
}

function signup(){
    x.style.left = "-400px";
    y.style.left = "50px";
    z.style.left = "110px";
}

function logini(){
    x.style.left = "50px";
    y.style.left = "450px";
    z.style.left = "0";
}

// Modali

document.getElementById('pointeri').addEventListener('click',function(){
    document.querySelector('.bg-modal').style.display = 'flex';
});

document.querySelector('.close').addEventListener('click', function(){
    document.querySelector('.bg-modal').style.display = 'none';
});
function validateForm() {

    var fillErrorName = document.getElementById("fillErrorName");
    var fillErrorEmail = document.getElementById("fillErrorEmail");
    var fillErrorKoment = document.getElementById("fillErrorKoment");

    var emri = document.forms["contactForm"]["emri"].value;
    var emriInput = document.getElementById("emri");

    var email = document.forms["contactForm"]["email"].value;
    var emailInput = document.getElementById("email");

    var koment = document.forms["contactForm"]["koment"].value;
    var komentInput = document.getElementById("koment");

    if (emri== null || emri.trim() == "") {
        fillErrorName.style.display = "block";
        emriInput.className = 'error';
        return false;
    }
    else{
        fillErrorName.style.display = "none";
        emriInput.className = '';
    }

    if(email== null || email.trim() == ""){
        fillErrorEmail.style.display = "block";
        emailInput.className = 'error';
        return false;
    }
    else{
        fillErrorEmail.style.display = "none";
        emailInput.className = '';
    }

    if(koment== null || koment.trim() == ""){
        fillErrorKoment.style.display = "block";
        komentInput.className = 'error';
        return false;
    }
    else{
        fillErrorKoment.style.display = "none";
        komentInput.className = '';
    }
}
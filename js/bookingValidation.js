
function checkforblankDestination() {
    var fillErrorDest= document.querySelector(".fillErrorDest");

    // destinacioni
    var destinacioni = document.getElementById('destinacioni');
    var invalidDestinacion = destinacioni.value == "Zgjedh destinacionin";

 
    if (invalidDestinacion) {
        fillErrorDest.style.display = "block";
        destinacioni.className = 'error';
    }
    else {
        destinacioni.className = '';
        fillErrorDest.style.display = "none";
    }
    
    return !invalidDestinacion;
}

function checkforblankHotel() {
    var fillErrorHot= document.querySelector(".fillErrorHot");

    // hoteli
    var hoteli = document.getElementById('hoteli');
    var invalidHotel= hoteli.value == "Zgjedh hotelin";
 
    if (invalidHotel) {
        fillErrorHot.style.display = "block";
        hoteli.className = 'error';
    }
    else {
        fillErrorHot.style.display = "none";
        hoteli.className = '';
    }
    
    return !invalidHotel;
}

function checkforblankMuaji() {
    var fillErrorM= document.querySelector(".fillErrorM");

    // muaji
    var muaji = document.getElementById('muaji');
    var invalidMuaji= muaji.value == "Zgjedh muajin";
 
    if (invalidMuaji) {
        fillErrorM.style.display = "block";
        muaji.className = 'error';
    }
    else {
        fillErrorM.style.display = "none";
        muaji.className = '';
    }
    
    return !invalidMuaji;
}

function checkforblankDita() {
    var fillErrorD= document.querySelector(".fillErrorD");

    // dita
    var dita = document.getElementById('dita');
    var invalidDita= dita.value == "Zgjedh diten";
 
    if (invalidDita) {
        fillErrorD.style.display = "block";
        dita.className = 'error';
    }
    else {
        fillErrorD.style.display = "none";
        dita.className = '';
    }
    
    return !invalidDita;
}

function checkforblank(){
    return checkforblankDestination() && checkforblankHotel() && checkforblankMuaji() && checkforblankDita();
}

var images = [
    "images/Optimized-aerial.jpg", 
    "images/Optimized-sea.jpg",
    "images/Optimized-palm.jpg", 
    "images/Optimized-island.jpg",
];

var num = 0;
  
function next() {
    var slider = document.getElementById("slider");
    num++;
    if(num >= images.length) {
      num = 0;
    }
    slider.src = images[num];
}
  
function prev() {
    var slider = document.getElementById("slider");
    num--;
    if(num < 0) {
      num = images.length-1;
    }
    slider.src = images[num];
}
